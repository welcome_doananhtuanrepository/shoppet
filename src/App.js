import './App.css';
import {BrowserRouter as Router, Routes,Route} from "react-router-dom"
import Header from "./component/Home/Header"
import DogPages from './component/Dogs/DogPages';
import CheckOut from './component/Cart/Cart';
import NavBar from './component/NavBar/NavBar';
import { useEffect, useState } from 'react';
import axios from "axios";
import {DogContext} from "./component/context/dogContext"

function App() {
  const [alldogs,setAlldogs]=useState([])
  const [mycart,setMycart]=useState([{}])
  const [total,setTotal]=useState(0)
    useEffect(()=>{
        async function getData(){
              const res=await axios.get("https://62fb4582e4bcaf5351806be5.mockapi.io/dogs")
              return res
          }
          getData().then(res=>setAlldogs(res.data))
          getData().catch(error=>alert("error"))
    },[])
  return (
    <DogContext.Provider value={{mycart,setMycart,total,setTotal}}>
      <Router>
        <NavBar />
        <div className='page-container'>
          <Routes>
            <Route path="/" element={<Header />} />
            <Route path="/Dogs" element={<DogPages alldogs={alldogs} />} />
            <Route path="/Checkout" element={<CheckOut />} />
          </Routes>
        </div>
      </Router>
    </DogContext.Provider>
  );
}


export default App;
