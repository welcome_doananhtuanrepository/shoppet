import React from "react";
import { useContext } from "react";
import { DogContext } from "../context/dogContext";
import {useNavigate} from "react-router-dom"
import "./cart.css"
const CheckOut=()=>{
    const {mycart,total,setTotal,setMycart}=useContext(DogContext)
    const navigate=useNavigate()
    const handleCheckOut=()=>{
        setTotal(0)
        setMycart([{}])
    }
    const handleHome=()=>{
        navigate("/")
    }
    return (
        <section className="cart-container">
            <div className="cart-header">Checkout</div>
             <div className="cart-items">
                 {mycart.slice(1).map((item,index)=>(
                    <div key={index} className="cart-item">
                        <img src={item.imageURL} className="cart-item-img" alt=""/>
                        {item.name}:{item.price}$
                    </div>
                ))}
                <div className="cart-total">Total: {total}$</div>
            </div>
            <button className="cart-checkout" onClick={handleCheckOut}>DONE</button>
            <button className="cart-checkout" onClick={handleHome}>Go Home</button>
        </section>
    )
}

export default CheckOut 