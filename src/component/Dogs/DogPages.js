import React from "react";
import DogCarts from "./DogsCart";
import "./dogs.css"
const DogPages=({alldogs})=>{
    console.log(alldogs)
    return (
        <section className="dogs-container">
            {alldogs.map((dog,index)=>
                <div key={index}>
                    <DogCarts 
                        id={dog.id}
                        name={dog.name}
                        breed={dog.breed}
                        description={dog.description}
                        price={dog.price}
                        imageURL={dog.image}
                    />
                </div>
                
            )}
        </section>
    )
}

export default DogPages