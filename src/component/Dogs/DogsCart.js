import React, { useRef, useState } from "react";
import "./dogs.css"
import { useContext } from "react";
import { DogContext } from "../context/dogContext";

const DogCarts=(props)=>{
    
    const {setMycart,setTotal}=useContext(DogContext)
    const [isAdded,setIsAdded]=useState(false)
    const {name,breed,description,price,imageURL}=props
    
    const handleClick=()=>{
        setIsAdded(!isAdded)
        const newItem={
            name:name,  
            price:price,
            imageURL:imageURL
        }
        setMycart(item=>[...item,newItem])
        setTotal(total=>total+=Math.floor(price*1))
    }
    return (
        <section className="dogs">
            <div className="dogs-inf">
                <p>{name}</p>
                <p>{breed}</p>
            </div>
            <div className="dogs-img-container">
                <img className="dog-img" src={imageURL} alt={`picture of ${name}`}></img>
            </div>
            <div className="dogs-desc">{description}</div>
            <div className="dogs-price">{price}</div> 
            
           
            <button disabled onClick={handleClick} style={{display:`${isAdded?"block":"none"}`}} className="dogs-btn">ADDED</button>
            
            <button onClick={handleClick} style={{display:`${isAdded?"none":"block"}`}} className="dogs-btn">ADD TO CART</button>
            
            
        </section>
    )
}

export default DogCarts