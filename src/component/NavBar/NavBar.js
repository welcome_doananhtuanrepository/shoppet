import React from "react";
import {Link} from "react-router-dom";
import "./navbar.css"
const NavBar=()=>{
    return (
        <div className="navbar">
            <Link to ="/">Home</Link>
            <Link to ="/Dogs">Dogs</Link>
            <Link to ="/Checkout">Checkout</Link>
        </div>
    )
}

export default NavBar